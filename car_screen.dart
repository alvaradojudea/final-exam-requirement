import 'package:flutter/material.dart';
import 'package:flutter_application_2/widgets/cart_item_samples.dart';

class CartScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back_ios_new,
                        size: 22,
                      ),
                    ),
                    Text(
                      "Cart",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.more_horiz,
                        size: 22,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 15),
                child: Column(
                  children: [
                    CartItemSamples(),
                    SizedBox(height: 50),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Select All", style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w500,
                          ),
                          ),
                          Checkbox(
                            activeColor: Color(0xFFFd725A),
                            value: true,
                            onChanged: (value) {                              
                            },
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 20),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Total Payment:",
                   style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w600
                  ),
                ),
                Text(
                  "\$598.62",
                  style: TextStyle(
                     color: Color(0xFFFD725A),
                     fontSize: 20,
                    fontWeight: FontWeight.w600
                  ),
                ),
              ],
            ),
                    ),
                     SizedBox(height: 20),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Delivery Cost:",
                   style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w600
                  ),
                ),
                Text(
                  "\$50.54",
                  style: TextStyle(
                     color: Color(0xFFFD725A),
                     fontSize: 20,
                    fontWeight: FontWeight.w600
                  ),
                ),
              ],
            ),
                    ),
                     SizedBox(height: 30),
             InkWell(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(
                  builder: (context)=> CartScreen(),
                ));
              },
                child: Container(
                padding: EdgeInsets.symmetric(
                vertical: 18, horizontal: 70),
                decoration: BoxDecoration(
                     color: Color(0xFFFD725A),
                     borderRadius: BorderRadius.circular(30),
                ),
                child: Text(
                  " Checkout",
                  style: TextStyle(
                  fontSize: 17,
                  fontWeight:FontWeight.w600,
                  letterSpacing: 1,
                  color: Colors.white.withOpacity(0.9)
                ),
              ),
             ),
            ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}